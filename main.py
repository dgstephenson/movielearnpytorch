import numpy as np
import pandas as pd
import torch
from torch import mean, std
import torch.optim as optim
import torch.nn as nn

# Import the Data
movieData = pd.read_csv('data/movieData.csv')
title = np.array(movieData["title"])
years = torch.tensor(movieData["year"]).float()
minutes = torch.tensor(movieData["minutes"]).float()
ratings = torch.tensor(movieData["rating"]).float()

# Define the Normalization Functions
def normalizeYears(x): return (x-mean(years))/std(years)
def normalizeMinutes(x): return (x-mean(minutes))/std(minutes)

# Separate Training and Testing Data
ids = np.arange(title.size)
np.random.shuffle(ids)
trainIds = ids[0:60000]
testIds = ids[60000:]
trainYears = years[trainIds,]
trainMinutes = minutes[trainIds,]
trainRatings = ratings[trainIds,]
testYears = years[testIds,]
testMinutes = minutes[testIds,]
testRatings = ratings[testIds,]

# Define the Model
class Model(nn.Module):
    def __init__(self):
        super(Model,self).__init__()
        self.layer1 = nn.Linear(in_features=2,out_features=2)
        self.layer2 = nn.Linear(in_features=2,out_features=1)
    def forward(self,years,minutes):
        normYears = normalizeYears(years)
        normMinutes = normalizeMinutes(minutes)
        inputs = torch.stack((normYears,normMinutes),dim=1)
        output1 = torch.sigmoid(self.layer1(inputs))
        output2 = self.layer2(output1)
        return output2


# Define the Loss Function
def getMeanSquaredError(predictions,targets):
    error = predictions-targets.unsqueeze(1)
    return torch.mean(error*error)

# Initialize the Model
model = Model()

# Initialize the Optimizer
optimizer = optim.Adam(model.parameters())

# Train the Model
print("Training...")
for step in range(7000):
    trainPredictions = model(trainYears,trainMinutes)
    trainLoss = getMeanSquaredError(trainPredictions,trainRatings)
    optimizer.zero_grad()
    trainLoss.backward()
    optimizer.step()
    testPredictions = model(testYears,testMinutes)
    testLoss = getMeanSquaredError(testPredictions,testRatings)
    if step % 100 == 0:
        print('step:',step,
              'trainLoss:',np.round(trainLoss.item(),3),
              'testLoss:',np.round(testLoss.item(),3))

# Print the Estimated Parameters
for name, parameter in model.named_parameters():
    print('')
    print(name)
    print(parameter.data)